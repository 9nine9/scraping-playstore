### Required
1. PHP
2. Web Server (for UI Dashboard) 
3. Composer (optional)

### How To Use
1. Download or clone in your local server
2. Put the files from `source_code` folder to your local server
3. (Optional) Write command `composer dump-autoload` in your project's folder

### API
1. UI Dashboard 
	- GET `/store/web/home`

2. API Scraping Playstore & Appstore (generate CSV)
	- POST `/store/excel`
	- Form: `text` (string, delimiter by `\n`)
	- Output: file .CSV
	- Ex Form: 
```
		text=
		https://play.google.com/store/apps/details?id=com.whatsapp\n
		https://apps.apple.com/id/app/mobile-legends-bang-bang/id1160056295
```
	

3. API Scraping Playstore & Appstore (output JSON)
	- GET `/store/read`
	- Param: `u` (string)
	- Output: JSON
	- Ex: 
```
		GET /store/read?u=https://apps.apple.com/id/app/mobile-legends-bang-bang/id1160056295
		GET /store/read?u=https://play.google.com/store/apps/details?id=com.whatsapp
```
Format JSON Output
```
{
  "store_type": "AppStore",
  "app_id": "mobile-legends-bang-bang/id1160056295",
  "name": "Mobile Legends: Bang Bang",
  "description": "Tantang Pemain Seluruh Dunia‪!‬",
  "category": "Game",
  "image": "",
  "author": "Shanghai Moonton Technology Co., Ltd.",
  "rating_content": "12+",
  "rating_star": 4.4,
  "rating_review": 947039,
  "price_value": "0",
  "price_currency": "IDR",
  "about": "",
  "updated_date": "7 Nov 2023",
  "whats_new": "",
  "data_permission": [
    "Konten Pengguna",
    "Pengenal",
    "Diagnostik",
    "Lokasi"
  ],
  "website": "https://www.facebook.com/MobileLegendsGameIndonesia/",
  "email": "",
  "policy": "https://m.mobilelegends.com/en/newsdetail/475",
  "uri": "https://apps.apple.com/id/app/mobile-legends-bang-bang/id1160056295"
}
```