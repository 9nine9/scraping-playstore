var Request = function(){
	var data;
	var is_valid = true;
	var object = {};

	this.input = function(string = null){
		data = string;
		return this;
	}

	this.makeValid = function(filter){
		/* email|url|required */
        
        if(is_valid){
            var error = false;

            var validation = filter.split('|');

            for(var i = 0; i < validation.length; i++){
                switch (validation[i]) {
                    case 'required':
                        error = isEmpty(data);
                        break;
                    case 'email':
                        error = !isMail(data);
                        break;
                    default:
                        //# code...
                        break;
                }
                
                if(error){
                    is_valid = false;
                    break;
                }
            }

        }

        return this;
	}

	this.save = function(){
		return data;
	}

	this.isValid = function(){
		return is_valid;
	}

	var isMail = function(string){
	    var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
	    return re.test(string);
	}

	var isEmpty = function(string){
	   	return (!string || 0 === string.length);
	}

	this.setData = function(form = []){
		object = {};

		is_valid = true;
    	for(var i = 0; i < form.length; i++){
    		if(object[form[i].required]){
    			if(isEmpty(form[i].value)){
    				is_valid = false;
    			}
    		}
      		object[form[i].name] = form[i].value;
		}
	}

	this.getData = function(){
		return object;
	}

	this.setEmpty = function(form = []){
    	for(var i = 0; i < form.length; i++){
      		form[i].value = '';
		}
	}


  	this.formatNumber = function(num){
    	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  	}
  

}