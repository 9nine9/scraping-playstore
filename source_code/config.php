<?php
define ('_CONFIG_', [
	
	/* -------------------------------
	 * Config time
	 * -------------------------------
	 */
	'timezone' => date_default_timezone_set('Asia/Jakarta'),
	'now' => date("Y-m-d H:i:s"),
	'debug' => true,

	/* -------------------------------
	 * Config database
	 * -------------------------------
	 */
	'db' => [
		'driver' => 'mysql',
		'host' => 'localhost',
		'port' => '3306',
		'username' => 'root',
		'password' => '',
		'dbname' => 'dbname',
		'debug_api_history' => false,
 	],
	
	/* -------------------------------
	 * Config token
	 * -------------------------------
	 */
	'token' => [
		'headers' => [
			'alg' => 'SHA256',
			'typ' => 'JWT'
		],
		'expired' => [
			'hours' => 24,
			'minutes' => 0,
			'seconds' => 0,
		],
		'key' => 'secret_key'

	],

]);