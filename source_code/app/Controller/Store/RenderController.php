<?php
namespace App\Controller\Store;

class RenderController
{
    protected $app;
    private $base;
    
    // constructor receives container instance
    public function __construct($c) {
        $this->app = $c;
        $this->base = 'store/';
    }
    
    public function home($request, $response, $args) {
        $name = 'Home';
        $url = 'home.html';
        
        return $this->app->view->render($response, $this->base . $url, [
            'name' => $name, 'version' => substr(time(), -4)
        ]);
    }
}

