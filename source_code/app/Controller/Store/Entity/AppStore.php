<?php
namespace App\Controller\Store\Entity;

use simplehtmldom\HtmlWeb;

class AppStore
{    

    public function __construct(){ 
        ini_set('max_execution_time', '0');
        ini_set('memory_limit', '-1');
    }

    public function getData($app_id){
        $result = [];
        $result['store_type'] = 'AppStore';
        $result['app_id'] = $app_id;
        $result['name'] = null;
        $result['description'] = null;
        $result['category'] = null;
        $result['image'] = null;
        $result['author'] = null;

        $result['rating_content'] = null;
        $result['rating_star'] = null;
        $result['rating_review'] = null;
        
        $result['price_value'] = null;
        $result['price_currency'] = null;

        $result['about'] = null;
        $result['updated_date'] = null;
        $result['whats_new'] = null;
        $result['data_permission'] = [];

        $result['website'] = null;
        $result['email'] = null;
        $result['policy'] = null;

        $link = "https://apps.apple.com/id/app/".$app_id."?l=id";
        $html_data = (new HtmlWeb())->load($link,false);
        if($html_data === null || $html_data === false) return $result;

        $script = $html_data->find('html', 0);
        $json1 = explode('<script name="schema:software-application" type="application/ld+json">',$script);
        if(count($json1) < 2) return $result;
        $json2 = explode('</script>', $json1[1]);
        if(count($json2) < 1) return $result;

        $data = json_decode($json2[0]);
        if($data == null) return $result;

        $description = '';
        foreach($html_data->find('h2.product-header__subtitle') as $e){
            $description = trim($e->plaintext);
            break;
        }

        $rating_content = '';
        foreach($html_data->find('span.badge--product-title') as $e){
            $rating_content = trim($e->plaintext);
            break;
        }

        $updated_date = '';
        foreach($html_data->find('div.whats-new__content time') as $e){
            $updated_date = trim($e->plaintext);
            break;
        }

        $whats_new = '';
        foreach($html_data->find('div.whats-new__content p[dir="false"]') as $e){
            $whats_new = trim($e->plaintext);
            break;
        }

        $price = '';
        foreach($html_data->find('li.app-header__list__item--price') as $e){
            $price = trim($e->plaintext);
            $price = str_replace('Rp ','',$price);
            $price = str_replace('ribu','000',$price);
            $price = str_replace('Gratis','0',$price);
            break;
        }

        $website = '';
        $email = '';
        $policy = '';
        foreach($html_data->find('ul.inline-list--app-extensions li') as $e){
            $e1 = $e->find('a',0);
            $temp = trim(strtolower($e1));
            if(preg_match('/\bsitus\b/', $temp)){
                $website = trim($e1->href);
            }
            else if(preg_match('/\bdukungan\b/', $temp)){
                if($website == '') $website = trim($e1->href);
            }
            else if(preg_match('/\bprivasi\b/', $temp)){
                $policy = trim($e1->href);
            }
        }

        $rating_star = isset($data->aggregateRating->ratingValue) ? 
            $data->aggregateRating->ratingValue : 0;

        $rating_review = isset($data->aggregateRating->ratingValue) ? 
            $data->aggregateRating->reviewCount : 0;

        $result['name'] = $data->name;
        $result['description'] = $description;
        $result['category'] = $data->applicationCategory;
        $result['image'] = $data->image;
        $result['author'] = $data->author->name;

        $result['rating_content'] = $rating_content;
        $result['rating_star'] = round($rating_star,1);
        $result['rating_review'] = (double) $rating_review;
        
        $result['price_value'] = $price;
        $result['price_currency'] = 'IDR';

        $result['about'] = $data->description;
        $result['updated_date'] = $updated_date;
        $result['whats_new'] = $whats_new;
        $result['data_permission'] = $this->_getPermission($html_data);
        
        $result['website'] = $website;
        $result['email'] = $email;
        $result['policy'] = $policy;

        return $result;
    }


    private function _getPermission($html_data){
        $result = [];
        foreach($html_data->find('ul.privacy-type__items li') as $e){
            $result[] = $e->find('span.privacy-type__data-category-heading',0)->plaintext;
        }

        return $result;
    }

}