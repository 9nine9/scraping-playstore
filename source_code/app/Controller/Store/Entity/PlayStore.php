<?php
namespace App\Controller\Store\Entity;

use simplehtmldom\HtmlWeb;

class PlayStore
{    

    public function __construct(){ 
        ini_set('max_execution_time', '0');
        ini_set('memory_limit', '-1');
    }

    public function getData($app_id){
        $result = [];
        $result['store_type'] = 'PlayStore';
        $result['app_id'] = $app_id;
        $result['name'] = null;
        $result['description'] = null;
        $result['category'] = null;
        $result['image'] = null;
        $result['author'] = null;

        $result['rating_content'] = null;
        $result['rating_star'] = null;
        $result['rating_review'] = null;
        
        $result['price_value'] = null;
        $result['price_currency'] = null;

        $result['about'] = null;
        $result['updated_date'] = null;
        $result['whats_new'] = null;
        $result['data_permission'] = [];

        $result['website'] = null;
        $result['email'] = null;
        $result['policy'] = null;

        $link = "https://play.google.com/store/apps/details?hl=id_ID&id=".$app_id;
        $html_data = (new HtmlWeb())->load($link,false);
        if($html_data === null || $html_data === false) return $result;

        $script = $html_data->find('html', 0);
        $json1 = explode('<script type="application/ld+json"', $script);
        if(count($json1) < 2) return $result;
        $json2 = explode('</script>', $json1[1]);
        if(count($json2) < 1) return $result;
        $json3 = explode('">', $json2[0]);
        if(count($json2) < 2) return $result;

        $data = json_decode($json3[1]);
        if($data == null) return $result;

        $updated_date = '';
        foreach($html_data->find('div.xg1aie') as $e){
            $updated_date = trim($e->plaintext);
            break;
        }

        $whats_new = '';
        foreach($html_data->find('div[itemprop="description"]') as $e){
            $whats_new = trim($e->plaintext);
            break;
        }

        $about = '';
        foreach($html_data->find('div.bARER') as $e){
            $about = trim($e->plaintext);
            break;
        }

        $website = '';
        $email = '';
        $policy = '';
        foreach($html_data->find('div.VfPpkd-EScbFb-JIbuQc a.RrSxVb') as $e){
            $temp = trim($e->find('i',0)->plaintext);
            if(strtolower($temp) == 'public'){
                $website = trim($e->href);
            }
            else if(strtolower($temp) == 'email'){
                $email = str_replace('mailto:','',trim($e->href));
            }
            else if(strtolower($temp) == 'shield'){
                $policy = trim($e->href);
            }
        }

        $result['name'] = $data->name;
        $result['description'] = $data->description;
        $result['category'] = $data->applicationCategory;
        $result['image'] = $data->image;
        $result['author'] = $data->author->name;

        $result['rating_content'] = str_replace('Rating ','',$data->contentRating);
        $result['rating_star'] = round($data->aggregateRating->ratingValue,1);
        $result['rating_review'] = (double) $data->aggregateRating->ratingCount;
        
        $result['price_value'] = $data->offers[0]->price;
        $result['price_currency'] = $data->offers[0]->priceCurrency;

        $result['about'] = $about;
        $result['updated_date'] = $updated_date;
        $result['whats_new'] = $whats_new;
        $result['data_permission'] = $this->_getPermission($app_id);

        $result['website'] = $website;
        $result['email'] = $email;
        $result['policy'] = $policy;

        return $result;
    }


    private function _getPermission($app_id){
        $result = [];
        $link = "https://play.google.com/store/apps/datasafety?hl=id_ID&id=".$app_id;
        $html_data = (new HtmlWeb())->load($link,false);
        if($html_data === null || $html_data === false) return $result;

        foreach($html_data->find('div[jscontroller="ojPjfd"]') as $e){
            $result[] = $e->find('h3.aFEzEb',0)->plaintext;
        }

        return $result;
    }

}