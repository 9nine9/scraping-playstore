<?php
namespace App\Controller\Store;

use App\Controller\Store\Entity\AppStore;
use App\Controller\Store\Entity\PlayStore;

class StoreController
{    

    public function __construct(){
        ini_set('max_execution_time', '0');
        ini_set('memory_limit', '-1');
    }

    public function read($request, $response, $args){
    	$u = $request->getParams()['u'];

        $appstore = new AppStore;
        $playstore = new PlayStore;

        $uri = strtolower($u);
        $res = null;

        if(preg_match('/\bgoogle\b/', $uri)){
        	//playstore
        	$app_id = explode('?id=', $uri);
        	if(count($app_id) < 2) return null;
        	$app_id = explode('&', $app_id[1])[0];

    		$res = $playstore->getData($app_id);
    		$res['uri'] = $uri;
    	}
        else if(preg_match('/\bapple\b/', $uri)){
			//appstore
        	$app_id = explode('/app/', $uri);
        	if(count($app_id) < 2) return null;
        	$app_id = explode('?', $app_id[1])[0];
        		
    		$res = $appstore->getData($app_id);
    		$res['uri'] = $uri;
    	}

        return $response->withJson($res);
    }

    public function excel($request, $response, $args){
        $appstore = new AppStore;
        $playstore = new PlayStore;

        $data = $request->getParsedBody();
        $text = $data['text'];
        $uri_list = (array) explode("\n", $text);

       	if(count($uri_list) == 0 || trim($uri_list[0]) == '') {
       		return $response->withJson(['message' => 'Failed - No url'],500);
       	}
       	if(count($uri_list) > 100) {
       		return $response->withJson(['message' => 'Failed - Max 100 url'],500);
       	}

        $result = [];
        for($i=0;$i<count($uri_list);$i++){
        	$uri = trim(strtolower($uri_list[$i]));
        	if($uri == '') continue;

        	if(preg_match('/\bgoogle\b/', $uri)){
        		//playstore
        		$app_id = explode('?id=', $uri);
        		if(count($app_id) < 2) continue;
        		$app_id = explode('&', $app_id[1])[0];

    			$res = $playstore->getData($app_id);
    			$res['uri'] = $uri;
    			$result[] = $res;
			}
        	else if(preg_match('/\bapple\b/', $uri)){
				//appstore
        		$app_id = explode('/app/', $uri);
        		if(count($app_id) < 2) continue;
        		$app_id = explode('?', $app_id[1])[0];
        		
    			$res = $appstore->getData($app_id);
    			$res['uri'] = $uri;
    			$result[] = $res;
			}

        }

        // return $response->withJson($result);
        return $response->withJson($this->_generateExcel($result));
    }

    private function _generateExcel($result){
        if(count($result) == 0) return ['message' => 'no data'];

        $header = [
            'Uri',
            'Store',
            'App ID',
            'Name',
            'Desc',
            'Author',
            'Category',
            'Image',

            'Website',
            'Email',
            'Policy',

            'Rating Content',
            'Rating Star',
            'Rating Review',

            'Price Value',
            'Price Currency',

            'About',
            'Updated',
            'Whats New',
            'Permission',
        ];

        $file = './assets/store_data.csv';
        $fp = fopen($file, 'w');
        fputcsv($fp, $header, ';');

        for($i=0;$i<count($result);$i++){
            $cell = (array) $result[$i];

            $list = [
                $cell['uri'], 
                $cell['store_type'], 
                $cell['app_id'], 
                $cell['name'], 
                $cell['description'], 
                $cell['author'],
                $cell['category'], 
                $cell['image'], 

                $cell['website'], 
                $cell['email'], 
                $cell['policy'], 

                $cell['rating_content'], 
                $cell['rating_star'], 
                $cell['rating_review'], 

                $cell['price_value'], 
                $cell['price_currency'], 

                $cell['about'], 
                $cell['updated_date'], 
                $cell['whats_new'], 
                implode($cell['data_permission'],','), 
            ];
            
            fputcsv($fp, $list, ';');
        }
        
        fclose($fp);

        return ['message' => 'Generate '.$file];
    }

}