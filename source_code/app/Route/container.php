<?php
use App\Database\DB;
use App\Controller;

$configuration = [
    'settings' => [
        'displayErrorDetails' => isset(_CONFIG_['debug']) ? _CONFIG_['debug'] : false, //disable in production
    ],
];

$container = new \Slim\Container($configuration);

$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig('app/Template', ['cache' => false]);
    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $c->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $basePath));
    return $view;
};

$container['Store\StoreController'] = function ($c) { 
    return new Controller\Store\StoreController(); 
};
$container['Store\RenderController'] = function ($c) { 
    return new Controller\Store\RenderController($c); 
};