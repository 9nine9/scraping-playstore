<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app = new \Slim\App($container);

$app->group('/store', function() use ($app) {
	$app->group('/web', function() use ($app) {
        $this-> get('/home', Store\RenderController::class . ':home');
    });

    $this-> get('/read', Store\StoreController::class . ':read');
    $this-> post('/excel', Store\StoreController::class . ':excel');
});


//run App
$app->run();

